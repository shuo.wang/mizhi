# MIZHI React + Redux

> React https://facebook.github.io/react/docs/hello-world.html  
> Redux http://redux.js.org/  
> 中文 - 阮一峰 http://www.ruanyifeng.com/home.html  
> 慕课网 http://www.imooc.com/  

## 技术引用

1. webpack https://webpack.github.io/  
2. babel https://babeljs.io/   
3. css -> SASS/SCSS, LESS  
4. create-react-app  https://github.com/facebookincubator/create-react-app
5. npm
6. yarn
7. ESLint
8. ES6
9. JSX