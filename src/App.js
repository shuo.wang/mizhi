import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import axios from 'axios';
import Header from './components/header/index';
import HomePage from './components/home-page/index';
import NewsPage from './components/news-page/index';
import todoPage from './components/todo/index';
import './App.css';


class App extends Component {

    constructor(props) {
        super(props);
        axios.defaults.baseURL = 'http://192.168.1.148:8888/services';
        // 对认证 cookie 传递使用
        // axios.defaults.withCredentials = true;
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <Header/>
                    <Switch>
                        <Route exact path="/"
                               render={() => <Redirect to="/home"/>}/>
                        <Route path="/home" component={HomePage}/>
                        <Route path="/news" component={NewsPage}/>
                        <Route path="/todo" component={todoPage}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
