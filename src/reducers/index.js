import {combineReducers} from 'redux';
import todos from './todos';
import visibilityFilter from './visibilityFilter';


const MIZHIApp = combineReducers({
    visibilityFilter,
    todos
});

export default MIZHIApp;