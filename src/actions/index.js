import axios from 'axios';

let nextTodoId = 0;
export const addTodo = text => {
    return {
        type: 'ADD_TODO',
        id: nextTodoId++,
        text
    }
};

export const setVisibilityFilter = filter => {
    return {
        type: 'SET_VISIBILITY_FILTER',
        filter
    }
};

export const toggleTodo = id => {
    return {
        type: 'TOGGLE_TODO',
        id
    }
};

export const asyncLoadUserData = () => {

    return (dispatch) => {
        axios.get('/users/1.json').then(res => {
            console.log(res);
            let userData = res.data;
            dispatch(addTodo(userData.username));
        }).catch(err => {
            console.error(err);
        })
    }
};