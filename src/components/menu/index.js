import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './index.css';

const MENU_DATA = [
    {
        'text': 'HOME',
        'url': '/home'
    }, {
        'text': 'NEWS',
        'url': '/news'
    }, {
        'text': 'VIDEOS',
        'url': '/videos'
    }, {
        'text': 'COMMUNITY',
        'url': '/community'
    }, {
        'text': 'SUPPORT',
        'url': 'support'
    },
];


export default class Menu extends Component {

    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
        this.state = {
            menus: MENU_DATA
        }
    }

    handleOnClick() {
        this.setState({
            menus: MENU_DATA
        });
    }

    render() {
        const {
            data
        } = this.props;
        console.log('data', data);
        return (
            <div className="main-menu">
                <nav>
                    <ul>
                        {
                            data.map((item, index) => {
                                return (
                                    <li key={index}><a href={item.url}>{item.text}</a></li>
                                );
                            })
                        }
                    </ul>
                </nav>
            </div>
        );
    }
}


Menu.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired
        }).isRequired
    ).isRequired
};