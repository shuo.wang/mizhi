import React from 'react';
import Footer from './Footer';
import AddTodo from '../../containers/todo/AddTodo';
import VisibleTodoList from '../../containers/todo/VisibleTodoList';
import './index.css';

const ToDoApp = () => (
    <div className="todo-main">
        <AddTodo/>
        <VisibleTodoList/>
        <Footer/>
    </div>
);

export default ToDoApp;