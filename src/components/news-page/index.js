import React, {Component} from 'react';
import axios from 'axios';
import './index.css';

export default class NewsPage extends Component {

    constructor(props) {
        super(props);
        this.getUserData = this.getUserData.bind(this);
        this.state = {
            userData: {
                id: 0,
                username: '未有值'
            }
        };
    }

    componentDidMount() {
        this.getUserData();
    }

    getUserData() {
        let that = this;
        axios.get('/users/1.json').then(function (res) {
            console.log(res);
            let userData = res.data;
            that.setState({userData});
        }).catch(function (err) {
            console.error(err);
        })
    }

    render() {
        const {
            userData
        } = this.state;
        return (
            <div className="news-main">
                <div>id:{userData.id}</div>
                <div>username:{userData.username}</div>
            </div>
        );
    }
}