import React, {Component} from 'react';
import Menu from '../menu/index';
import './index.css';

// mock data
const MENU_DATA = [
    {
        'text': 'HOME',
        'url': '/home'
    }, {
        'text': 'TODO',
        'url': '/todo'
    }, {
        'text': 'NEWS',
        'url': '/news'
    }, {
        'text': 'VIDEOS',
        'url': '/videos'
    }, {
        'text': 'COMMUNITY',
        'url': '/community'
    }, {
        'text': 'SUPPORT',
        'url': '/support'
    },{
        'text': 'HELP',
        'url': '/help'
    }
];

export default class Header extends Component {

    render() {
        return (
            <header>
                <Menu data={MENU_DATA}/>
            </header>
        );
    }
}
